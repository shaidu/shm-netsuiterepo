﻿/// <reference path="../template/SHM_Journal_Entries_list.html" />
var oJournal_Entries = [];
var vOldTranid = 0;
var oJournalEntriesGroupByT = [];
var oJournalEntriesDetail = [];

var oDbs = [{ "Port": "6153", "Location": "62", "Company": "Brewer Dauntless Shipyard & Brewer Essex Island", "id": "2" }, { "Port": "6154", "Location": "65", "Company": "Brewer Ferry Point Marina", "id": "3" }, { "Port": "6155", "Location": "72", "Company": "Brewer Greenwich Bay Marina", "id": "4" }, { "Port": "6156", "Location": "76", "Company": "Brewer Glen Cove Marina", "id": "5" }, { "Port": "6157", "Location": "61", "Company": "Brewer Bruce & Johnson's Marina", "id": "6" }, { "Port": "6172", "Location": "84", "Company": "Brewer Plymouth Marina", "id": "7" }, { "Port": "6158", "Location": "73", "Company": "Brewer Sakonnet Marina", "id": "8" }, { "Port": "6159", "Location": "69", "Company": "Brewer Yacht Haven Marina", "id": "9" }, { "Port": "6152", "Location": "75", "Company": "Brewer Capri Marina", "id": "10" }, { "Port": "6160", "Location": "70", "Company": "Brewer Cove Haven Marina", "id": "11" }, { "Port": "6150", "Location": "71", "Company": "Brewer Cowesett Marina", "id": "12" }, { "Port": "6161", "Location": "63", "Company": "Brewer Deep River Marina", "id": "13" }, { "Port": "6153", "Location": "64", "Company": "Brewer Essex Island Marina", "id": "14" }, { "Port": "6162", "Location": "80", "Company": "Brewer Fiddler's Cove Marina", "id": "15" }, { "Port": "6163", "Location": "81", "Company": "Brewer Green Harbor Marina", "id": "16" }, { "Port": "6164", "Location": "79", "Company": "Brewer Yacht Yard at Greenport & Brewer Stirling Harbor", "id": "17" }, { "Port": "6165", "Location": "82", "Company": "Brewer Hawthorne Cove Marina", "id": "18" }, { "Port": "6166", "Location": "66", "Company": "Brewer Mystic River Marina", "id": "19" }, { "Port": "6167", "Location": "83", "Company": "Brewer Onset Bay Marina", "id": "20" }, { "Port": "6151", "Location": "85", "Company": "Brewer Oxford BY & Marina", "id": "21" }, { "Port": "6168", "Location": "67", "Company": "Brewer Pilots Point Marina", "id": "22" }, { "Port": "6169", "Location": "77", "Company": "Brewer Post Road Boat Yard", "id": "23" }, { "Port": "6170", "Location": "68", "Company": "Brewer Stratford Marina", "id": "24" }, { "Port": "6164", "Location": "78", "Company": "Brewer Stirling Harbor Marina", "id": "25" }, { "Port": "6171", "Location": "74", "Company": "Brewer Wickford Cove Marina", "id": "26" }];

var vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';

var vBullet_toggle_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=786&c=4519673&h=d837cc9ceeda6eb1b6f6&whence=';
var vBullet_toggle_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=785&c=4519673&h=153c3a1ae5dfdc594d0f&whence=';

var vDetailIcon_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=791&c=4519673&h=a5e026295a2f4d67480d&whence=';
var vDetailIcon_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=790&c=4519673&h=c0d9f55a389f8d729c23&whence=';

var vTemplateListingURL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=817&c=4519673&h=178823d3a697af5da469&mv=j2uf1e54&_xt=.html&whence='

if (window.location.href.indexOf('localhost') > 0) {
    vTemplateListingURL = 'template/shm-je-import.html';
    vNetSuiteURL = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=146&deploy=1';

    vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';


    vBullet_toggle_plus = 'images/bullet_toggle_plus.png';
    vBullet_toggle_minus = 'images/bullet_toggle_minus.png';

    vDetailIcon_plus = 'images/icon_plus.png';
    vDetailIcon_minus = 'images/icon_minus.png'
}

var url = '';

function GetJournal_Entries(pDatabase, pLocation, pFiscalYear, pFiscalPeriod, beginEffDate, endEffDate, cmdbid) {

    var vURL = vNetSuiteURL + '&mode=MTIJournalEntrieslist&database=' + pDatabase + '&fiscalyear=' + pFiscalYear + '&location=' + pLocation + '&fiscalperiod=' + pFiscalPeriod + '&beginEffDate=' + beginEffDate + '&endEffDate=' + endEffDate + '&cmdbid=' + cmdbid + '&callback=GetSHMJournalEntriesCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function DisplayJournal_Entries() {
    $("[id = 'div_Journal_Entries_list']").html('');
    $("[id = 'div_Journal_Entries_list']").setTemplateURL(vTemplateListingURL);
    $("[id = 'div_Journal_Entries_list']").processTemplate({ data: oJournal_Entries });
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'right'
    });
    $('table table').hide();
    $('td > a').on('click', function (e) {
        e.preventDefault();

        if ($(this).closest('tr').next().find('table:first').is(":visible")) {
            $(this).closest('tr').next().find('table:first').fadeOut(100);
            $(this).find('img').attr('src', vDetailIcon_plus);
        }
        else {
            $(this).closest('tr').next().find('table:first').fadeIn(100);
            $(this).find('img').attr('src', vDetailIcon_minus);
        }
    });

}

function ToggleDetails(pToggle) {
    if (pToggle) {
        $('table table').show();
        $('td > a >img').attr('src', vDetailIcon_minus);


    } else {
        $('table table').hide();
        $('td > a >img').attr('src', vDetailIcon_plus);
    }

}

function ToggleBtns(pToggle) {
    if (pToggle) {
        $("[id = 'btnExpand']").show();
        $("[id = 'btnCollapse']").show();
        $("[id = 'btnValidate']").show();
        $("[id = 'btnImport']").show();
    } else {
        $("[id = 'btnExpand']").hide();
        $("[id = 'btnCollapse']").hide();
        $("[id = 'btnValidate']").hide();
        $("[id = 'btnImport']").hide();
    }
}

function GetSHMJournalEntriesCallback(oData) {
    $("#wait").css("display", "none");
    oJournal_Entries = oData;
    if (oJournal_Entries.length > 0) {
        ToggleBtns(true);
    } else {
        ToggleBtns(false);
    }
    DisplayJournal_Entries();
}

function GetDbsCallBack(oData) {
    $("#wait").css("display", "none");
    oDbs = [{"Port":"6153","Location":"62","Company":"Brewer Dauntless Shipyard & Brewer Essex Island","id":"2"},{"Port":"6154","Location":"65","Company":"Brewer Ferry Point Marina","id":"3"},{"Port":"6155","Location":"72","Company":"Brewer Greenwich Bay Marina","id":"4"},{"Port":"6156","Location":"76","Company":"Brewer Glen Cove Marina","id":"5"},{"Port":"6157","Location":"61","Company":"Brewer Bruce & Johnson's Marina","id":"6"},{"Port":"6172","Location":"84","Company":"Brewer Plymouth Marina","id":"7"},{"Port":"6158","Location":"73","Company":"Brewer Sakonnet Marina","id":"8"},{"Port":"6159","Location":"69","Company":"Brewer Yacht Haven Marina","id":"9"},{"Port":"6152","Location":"75","Company":"Brewer Capri Marina","id":"10"},{"Port":"6160","Location":"70","Company":"Brewer Cove Haven Marina","id":"11"},{"Port":"6150","Location":"71","Company":"Brewer Cowesett Marina","id":"12"},{"Port":"6161","Location":"63","Company":"Brewer Deep River Marina","id":"13"},{"Port":"6153","Location":"64","Company":"Brewer Essex Island Marina","id":"14"},{"Port":"6162","Location":"80","Company":"Brewer Fiddler's Cove Marina","id":"15"},{"Port":"6163","Location":"81","Company":"Brewer Green Harbor Marina","id":"16"},{"Port":"6164","Location":"79","Company":"Brewer Yacht Yard at Greenport & Brewer Stirling Harbor","id":"17"},{"Port":"6165","Location":"82","Company":"Brewer Hawthorne Cove Marina","id":"18"},{"Port":"6166","Location":"66","Company":"Brewer Mystic River Marina","id":"19"},{"Port":"6167","Location":"83","Company":"Brewer Onset Bay Marina","id":"20"},{"Port":"6151","Location":"85","Company":"Brewer Oxford BY & Marina","id":"21"},{"Port":"6168","Location":"67","Company":"Brewer Pilots Point Marina","id":"22"},{"Port":"6169","Location":"77","Company":"Brewer Post Road Boat Yard","id":"23"},{"Port":"6170","Location":"68","Company":"Brewer Stratford Marina","id":"24"},{"Port":"6164","Location":"78","Company":"Brewer Stirling Harbor Marina","id":"25"},{"Port":"6171","Location":"74","Company":"Brewer Wickford Cove Marina","id":"26"}];
    for (i = 0; i < oData.length; i++) {
        oDbs.push({
            'Port': oData[i].fields.custrecordcustrecordnco_mytaskit_port,
            'Location': oData[i].fields.custrecordnco_mytaskit_location,
            'Company': oData[i].fields.custrecordnco_mytaskit_company,
            'id': oData[i].id,
        });
    };

   
    LoadDatabaseCombo();
}

function LoadDatabaseCombo() {
    $("[id='ddlDatabase']").off("chang");
    $("[id='ddlDatabase']").empty();

 
    for (i = 0; i < oDbs.length; i++) {
        $("[id='ddlDatabase']").append("<option value='" + oDbs[i].id + "'>" + oDbs[i].Company + "</option>");
    };
    //$("[id='ddlDatabase']").val("6152").attr("selected", "selected");
    $("[id='ddlDatabase']").val("3").attr("selected", "selected");
    $("[id='ddlDatabase']").attr("disabled","disabled");
    $("[id='ddlDatabase']").off("chang").on("change", function () {
        LoadLocationCombo($("[id='ddlDatabase']").val());
        ClearListing();
    });
    LoadLocationCombo($("[id='ddlDatabase']").val());

    $("[id='ddlLocation']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalYear']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalPeriod']").off("chang").on("change", function () {
        ClearListing()
    });
}

function ClearListing() {
    $("[id = 'div_Journal_Entries_list']").html('');
    ToggleBtns(false);
}

function LoadLocationCombo(vDbname) {
    $("[id='ddlLocation']").off("chang");
    $("#ddlLocation").empty();

    var oLoaction = Enumerable.From(oDbs).Where(function (x) {
        return x.id == vDbname
    }).Select(function (x) {
        return x.Location
    }).ToArray();
    oLoaction = $.unique(oLoaction);

    for (i = 0; i < oLoaction.length; i++) {
        $("#ddlLocation").append("<option value='" + oLoaction[i] + "'>" + oLoaction[i] + "</option>");
    };

}

function GetDbs() {

    var vURL = vNetSuiteURL + '&mode=GetRecordType&recordtype=customrecordnco_mytaskit_db_settings&callback=GetDbsCallBack';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (oData) {
            oDbs = oData;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetLastDayOfMonth() {
    var vYear = $("[id = 'ddlFiscalYear']").val();
    var vMonth = $("[id = 'ddlFiscalPeriod']").val();
    var vDate = new Date(vYear, vMonth, 0);
    var vLastday = vDate.toString().split(' ')[2];

    return vMonth + '/' + vLastday+'/'+vYear;
    
}

function GetMTIValidate(pDatabase, pLocation, pFiscalYear, pFiscalPeriod, beginEffDate, endEffDate) {

    var vURL = vNetSuiteURL + '&mode=MTIDataValidate&database=' + pDatabase + '&location=' + pLocation + '&fiscalperiod=' + pFiscalPeriod + '&beginEffDate=' + beginEffDate + '&endEffDate=' + endEffDate + '&callback=GetSHMJournalEntriesCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetMTIImport(pDatabase, pLocation, pFiscalYear, pFiscalPeriod, beginEffDate, endEffDate, pDbName, pLocationName) {
    var vURL = vNetSuiteURL + '&mode=MTIImport&database=' + pDatabase + '&location=' + pLocation + '&fiscalyear=' + pFiscalYear + '&fiscalperiod=' + pFiscalPeriod + '&beginEffDate=' + beginEffDate + '&endEffDate=' + endEffDate + '&databasename=' + pDbName + '&locationname=' + pLocationName + '&callback=GetMTIImportCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetMTIImportCallback(oData) {
    debugger;
    $("#wait").css("display", "none");
    $("[id = 'div-success']").css("display", "none");
    $("[id = 'div-error']").css("display", "none");

    if (oData.status == 0) {
        $("[id = 'div-success']").css("display", "");
        $("[id = 'div-success']").html(oData.message);
        setTimeout(function () { $("[id = 'div-success']").hide() }, 8000);
    } else {
        $("[id = 'div-error']").css("display", "");
        $("[id = 'div-error']").html(oData.message);
        setTimeout(function () { $("[id = 'div-error']").hide() }, 8000);
    }
}

$(document).ready(function () {
    GetDbs();
    $("[id='btnViewList']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        var endEffDate  = GetLastDayOfMonth();
        var beginEffDate = $("[id = 'ddlFiscalPeriod'] option:selected").val() + '/01/' + $("[id = 'ddlFiscalYear'] option:selected").val();

        GetJournal_Entries($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), beginEffDate, endEffDate);
    });

    $("[id='btnValidate']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        var endEffDate = GetLastDayOfMonth();
        var beginEffDate = $("[id = 'ddlFiscalPeriod']").val() + '/01/' + $("[id = 'ddlFiscalYear']").val();
        GetMTIValidate($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val(), beginEffDate, endEffDate);
    });

    $("[id='btnImport']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        //GetFSMbtnImport($("[id = 'ddlDatabase']").val(), $("[id = 'ddlLocation']").val(), $("[id = 'ddlFiscalYear']").val(), $("[id = 'ddlFiscalPeriod']").val());
        var endEffDate = GetLastDayOfMonth();
        var beginEffDate = $("[id = 'ddlFiscalPeriod']").val() + '/01/' + $("[id = 'ddlFiscalYear']").val();
        GetMTIImport($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val(), beginEffDate, endEffDate, $("[id = 'ddlDatabase'] option:selected").text(), $("[id = 'ddlLocation'] option:selected").text());

    });

    $("[id = 'btnExpand']").on('click', function (e) {
        ToggleDetails(true);
    });

    $("[id = 'btnCollapse']").on('click', function (e) {
        ToggleDetails(false);
    });

    $('#accordion').on('shown.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_minus);
    });

    $('#accordion ').on('hidden.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_plus);
    });

    //$(document).ajaxStart(function () {
    //    $("#wait").css("display", "block");
    //});
    //$(document).ajaxComplete(function () {
    //    $("#wait").css("display", "none");
    //});

});

function DisplayError(pErrorMessage) {
    alert(JSON.stringify(pErrorMessage));

}

function getFormattedDate(pDate) {
    var d = pDate.slice(0, 10).split('-');
    if (d.length > 1) {
        var year = d[0];
        var day = d[2];
        var month = d[1];
        //d[1] + '/' + d[2] + '/' + d[0];
        return month + '/' + day + '/' + year;
    }
    return pDate
}
