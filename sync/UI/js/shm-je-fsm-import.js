﻿
var vSelectedClient = 'FSM';

var oJournal_Entries = [];

var oDbs = [];

var vDisplayStep = 1;

var url = '';

var vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';
var vTemplateListingURL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=817&c=4519673&h=178823d3a697af5da469&mv=j2uf1e54&_xt=.html&whence=';

//images path
var vBullet_toggle_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=786&c=4519673&h=d837cc9ceeda6eb1b6f6&whence=';
var vBullet_toggle_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=785&c=4519673&h=153c3a1ae5dfdc594d0f&whence=';

var vDetailIcon_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=791&c=4519673&h=a5e026295a2f4d67480d&whence=';
var vDetailIcon_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=790&c=4519673&h=c0d9f55a389f8d729c23&whence=';



if (window.location.href.indexOf('localhost') > 0) {
    vTemplateListingURL = 'template/shm-je-import.html';
    //Hillas demo account
    vNetSuiteURL = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=146&deploy=1';

    vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';

    vBullet_toggle_plus = 'images/bullet_toggle_plus.png';
    vBullet_toggle_minus = 'images/bullet_toggle_minus.png';

    vDetailIcon_plus = 'images/icon_plus.png';
    vDetailIcon_minus = 'images/icon_minus.png';
}


function GetJournal_Entries(pDatabase, pLocation, pFiscalYear, pFiscalPeriod) {
    vDisplayStep = 1;
    var vURL = vNetSuiteURL + '&mode=JournalEntrieslist&database=' + pDatabase + '&location=' + pLocation + '&fiscalyear=' + pFiscalYear + '&fiscalperiod=' + pFiscalPeriod + '&callback=GetJournalEntriesCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetJournalEntriesCallback(oData) {
    $("#wait").css("display", "none");
    oJournal_Entries = oData;
    if (oJournal_Entries.length > 0) {
        ToggleBtns(true);
    } else {
        ToggleBtns(false);
    }
    DisplayJournal_Entries();
}

function GetFSMValidate(pDatabase, pLocation, pFiscalYear, pFiscalPeriod) {
    vDisplayStep = 2;
    var vURL = vNetSuiteURL + '&mode=FSMValidate&database=' + pDatabase + '&location=' + pLocation + '&fiscalyear=' + pFiscalYear + '&fiscalperiod=' + pFiscalPeriod + '&callback=GetJournalEntriesCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}




function GetFSMImport(pDatabase, pLocation, pFiscalYear, pFiscalPeriod, pDbName, pLocationName) {
    var vURL = vNetSuiteURL + '&mode=FSMImport&database=' + pDatabase + '&location=' + pLocation + '&fiscalyear=' + pFiscalYear + '&fiscalperiod=' + pFiscalPeriod + '&databasename=' + pDbName + '&locationname=' + pLocationName + '&callback=GetFSMImportCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetFSMImportCallback(oData) {
    $("#wait").css("display", "none");
    $("[id = 'div-success']").css("display", "none");
    $("[id = 'div-error']").css("display", "none");

    if (oData.status == 0) {
        $("[id = 'div-success']").css("display", "");
        $("[id = 'div-success']").html(oData.message);
        setTimeout(function () { $("[id = 'div-success']").hide() }, 8000);
    } else {
        $("[id = 'div-error']").css("display", "");
        $("[id = 'div-error']").html(oData.message);
        setTimeout(function () { $("[id = 'div-error']").hide() }, 8000);
    }
}

function GetDbs() {
    var vURL = vNetSuiteURL + '&mode=GetDbs&callback=GetDbsCallBack';// 'http://shmwebservice.esided.com/API/GetDbs?callback=GetDbsCallBack';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (oData) {
            oDbs = oData;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetDbsCallBack(oData) {
    oDbs = oData;
    LoadDatabaseCombo();
}

function DisplayJournal_Entries() {
    $("[id = 'div_Journal_Entries_list']").html('');
    $("[id = 'div_Journal_Entries_list']").setTemplateURL(vTemplateListingURL);
    $("[id = 'div_Journal_Entries_list']").processTemplate({ data: oJournal_Entries });
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'right'
    });
    $('table table').hide();
    $('td > a').on('click', function (e) {
        e.preventDefault();

        if ($(this).closest('tr').next().find('table:first').is(":visible")) {
            $(this).closest('tr').next().find('table:first').fadeOut(100);
            $(this).find('img').attr('src', vDetailIcon_plus);
        }
        else {
            $(this).closest('tr').next().find('table:first').fadeIn(100);
            $(this).find('img').attr('src', vDetailIcon_minus);
        }
    });

}


function ToggleDetails(pToggle) {
    if (pToggle) {
        $('table table').show();
        $('td > a >img').attr('src', vDetailIcon_minus);
 
 
    } else {
        $('table table').hide();
        $('td > a >img').attr('src', vDetailIcon_plus);
    }

}

function ToggleBtns(pToggle) {
    if (pToggle) {
        $("[id = 'btnExpand']").show();
        $("[id = 'btnCollapse']").show();
        $("[id = 'btnValidate']").show();
        $("[id = 'btnImport']").show();
    } else {
        $("[id = 'btnExpand']").hide();
        $("[id = 'btnCollapse']").hide();
        $("[id = 'btnValidate']").hide();
        $("[id = 'btnImport']").hide();
    }
}





function LoadDatabaseCombo() {
    $("[id='ddlDatabase']").off("chang");
    $("#ddlDatabase").empty();

    for (i = 0; i < oDbs.length; i++) {
        $("#ddlDatabase").append("<option value='" + oDbs[i].name + "'>" + oDbs[i].name + "</option>");
    };

    $("[id='ddlDatabase']").off("chang").on("change", function () {
        LoadLocationCombo($("[id='ddlDatabase']").val());
        ClearListing();
    });
    LoadLocationCombo($("[id='ddlDatabase']").val());

    $("[id='ddlLocation']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalYear']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalPeriod']").off("chang").on("change", function () {
        ClearListing()
    });
}

function ClearListing() {
    $("[id = 'div_Journal_Entries_list']").html('');
    ToggleBtns(false);
}

function LoadLocationCombo(vDbname) {
    $("[id='ddlLocation']").off("chang");
    $("#ddlLocation").empty();

    var oLoaction = Enumerable.From(oDbs).Where(function (x) {
        return x.name == vDbname
    }).Select(function (x) {
        return x.locations
    }).ToArray()[0];


    for (i = 0; i < oLoaction.length; i++) {
        $("#ddlLocation").append("<option value='" + oLoaction[i] + "'>" + oLoaction[i] + "</option>");
    };

}


$(document).ready(function () {
    GetDbs();
    $("[id='btnViewList']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        GetJournal_Entries($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val());
    });

    $("[id='btnValidate']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        GetFSMValidate($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val());
    });

    $("[id='btnImport']").off("click").on("click", function () {
        $("#wait").css("display", "block");
        GetFSMImport($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val(), $("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val());
    });

    $("[id = 'btnExpand']").on('click', function (e) {
        ToggleDetails(true);
    });

    $("[id = 'btnCollapse']").on('click', function (e) {
        ToggleDetails(false);
    });

    $('#accordion').on('shown.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_minus);
    });

    $('#accordion ').on('hidden.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_plus);
    });    //$(document).ajaxStart(function () {
    //    $("#wait").css("display", "block");
    //});
    //$(document).ajaxComplete(function () {
    //    $("#wait").css("display", "none");
    //});
 
});


function DisplayError(pErrorMessage) {
    alert(JSON.stringify(pErrorMessage));

}

function getFormattedDate(pDate) {
    var d = pDate.slice(0, 10).split('-');
    if (d.length > 1) {
        var year = d[0];
        var day = d[2];
        var month = d[1];
        //d[1] + '/' + d[2] + '/' + d[0];
        return month + '/' + day + '/' + year;
    }
    return pDate
}
