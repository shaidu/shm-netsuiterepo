﻿
var vSelectedClient = 'FSM';




var url = '';

var vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';
var vTemplateListingURL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=817&c=4519673&h=178823d3a697af5da469&mv=j2uf1e54&_xt=.html&whence='

//images path
var vBullet_toggle_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=786&c=4519673&h=d837cc9ceeda6eb1b6f6&whence=';
var vBullet_toggle_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=785&c=4519673&h=153c3a1ae5dfdc594d0f&whence=';

var vDetailIcon_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=791&c=4519673&h=a5e026295a2f4d67480d&whence=';
var vDetailIcon_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=790&c=4519673&h=c0d9f55a389f8d729c23&whence=';



if (window.location.href.indexOf('localhost') > 0) {
    vSHM_Import_TemplateURL = 'template/shm-je-import.html';
    //Hillas demo account
    vNetSuiteURL = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=146&deploy=1';

    vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';

    vTemplateListingURL = "template/shm-je-import.html";


    vBullet_toggle_plus = 'images/bullet_toggle_plus.png';
    vBullet_toggle_minus = 'images/bullet_toggle_minus.png';

    vDetailIcon_plus = 'images/icon_plus.png';
    vDetailIcon_minus = 'images/icon_minus.png';
}


function GetJournal_Entries(pFileId) {

    var vURL = vNetSuiteURL + '&mode=GetImportLogFile&fileid=' + pFileId + '&callback=GetJournalEntriesCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetJournalEntriesCallback(oData) {
    oJournal_Entries = oData;

    DisplayJournal_Entries();
}





function DisplayJournal_Entries() {
    $("[id = 'div_Journal_Entries_list']").html('');
    $("[id = 'div_Journal_Entries_list']").setTemplateURL(vTemplateListingURL);
    $("[id = 'div_Journal_Entries_list']").processTemplate({ data: oJournal_Entries });
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'right'
    });
    $('table table').hide();
    $('td > a').on('click', function (e) {
        e.preventDefault();

        if ($(this).closest('tr').next().find('table:first').is(":visible")) {
            $(this).closest('tr').next().find('table:first').fadeOut(100);
            $(this).find('img').attr('src', vDetailIcon_plus);
        }
        else {
            $(this).closest('tr').next().find('table:first').fadeIn(100);
            $(this).find('img').attr('src', vDetailIcon_minus);
        }
    });

}
 

function ToggleDetails(pToggle) {
    if (pToggle) {
        $('table table').show();
        $('td > a >img').attr('src', vDetailIcon_minus);
 
 
    } else {
        $('table table').hide();
        $('td > a >img').attr('src', vDetailIcon_plus);
    }

}




function GetFileId() {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == "fileid") {
            return sParameterName[1];
        }
    }
    return 0;
}

$(document).ready(function () {


    var vFileId = GetFileId();
    if (vFileId>1){
        GetJournal_Entries(vFileId);
    }
  

    $("[id = 'btnExpand']").on('click', function (e) {
        ToggleDetails(true);
    });

    $("[id = 'btnCollapse']").on('click', function (e) {
        ToggleDetails(false);
    });

    $('#accordion').on('shown.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_minus);
    });

    $('#accordion ').on('hidden.bs.collapse', function () {
        $('.panel-title > a > img').attr('src', vBullet_toggle_plus);
    });
});


function DisplayError(pErrorMessage) {
    alert(pErrorMessage);

}
function getFormattedDate(pDate) {
    var d = pDate.slice(0, 10).split('-');
    if (d.length > 1) {
        var year = d[0];
        var day = d[2];
        var month = d[1];
        //d[1] + '/' + d[2] + '/' + d[0];
        return month + '/' + day + '/' + year;
    }
    return pDate
}