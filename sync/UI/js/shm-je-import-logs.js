﻿
var vSelectedClient = 'FSM';

var oJELogs = [];

var oDbs = [];
var oMTIDbs = [];

var vPageNo = 0;
var vPageSize = 0;
var vTotalRecords = 0;
var oPageRanges = [];
var vPageCount = 0;

var url = '';

var vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';
var vSHM_Import_log_TemplateURL = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=818&c=4519673&h=a8aa9ccd7d39b6d8e07c&mv=j2uf1h88&_xt=.html&whence=';
                                   
//images path
var vBullet_toggle_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=786&c=4519673&h=d837cc9ceeda6eb1b6f6&whence=';
var vBullet_toggle_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=785&c=4519673&h=153c3a1ae5dfdc594d0f&whence=';

var vDetailIcon_plus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=791&c=4519673&h=a5e026295a2f4d67480d&whence=';
var vDetailIcon_minus = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=790&c=4519673&h=c0d9f55a389f8d729c23&whence=';

var vDetail_viwe_log_template = 'https://system.sandbox.netsuite.com/core/media/media.nl?id=810&c=4519673&h=b903d3576aede7046d7a&mv=j2uev428&_xt=.html&whence=&';


if (window.location.href.indexOf('localhost') > 0) {
    vSHM_Import_log_TemplateURL = 'template/shm-je-import-logs.html';
    vDetail_viwe_log_template = 'SHM-JE-Import-logs-view.html?'

    //Hillas demo account
    vNetSuiteURL = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=146&deploy=1';
    vNetSuiteURL = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1';

    vBullet_toggle_plus = 'images/bullet_toggle_plus.png';
    vBullet_toggle_minus = 'images/bullet_toggle_minus.png';

    vDetailIcon_plus = 'images/icon_plus.png';
    vDetailIcon_minus = 'images/icon_minus.png';
}


function GetGLLogs(pDatabase, pLocation, pFiscalYear, pFiscalPeriod, pFromDate, pToDate, pType) {
 
    var vURL = vNetSuiteURL + '&mode=GetImportLogsList&recordtype=customrecord_shm_ge_import_log&pageno=' + vPageNo + '&database=' + pDatabase + '&location=' + pLocation + '&fiscalyear=' + pFiscalYear + '&fiscalperiod=' + pFiscalPeriod + '&FromDate=' + pFromDate + '&ToDate=' + pToDate + '&Type=' + pType + '&callback=GetGLLogsCallback';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (data) {
            oJournal_Entries = data;
            DisplayJournal_Entries();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}


function GetGLLogsCallback(oData) {

    //vPageNo = oData.PageNo;
    vPageSize = oData.PageSize;
    vTotalRecords = oData.Total;
    oPageRanges = oData.PageRanges;
    vPageCount = oData.PageRanges.length;

    oJELogs = [];
    var vTempData = JSON.parse(oData.Data);
    for (x = 0; x < vTempData.length; x++) {
        var oFields = vTempData[x].fields;

        oJELogs.push(
            {
                custrecord_shm_fiscal_year: oFields.custrecord_shm_fiscal_year,
                created: oFields.created,
                custrecord_shm_location_id: oFields.custrecord_shm_location_id,
                custrecord_shm_file_id: oFields.custrecord_shm_file_id,
                custrecord_shm_file_url: oFields.custrecord_shm_file_url,
                custrecord_shm_database_id: oFields.custrecord_shm_database_id,
                custrecord_shm_fiscal_period: oFields.custrecord_shm_fiscal_period,

                custrecord_shm_database_name: oFields.custrecord_shm_database_name,
                custrecord_shm_location_name: oFields.custrecord_shm_location_name,
                custrecord_shm_total_entries: oFields.custrecord_shm_total_entries,
                custrecord_shm_total_successfull_entries: oFields.custrecord_shm_total_successfull_entries,
                custrecord_shm_total_failed_entries: oFields.custrecord_shm_total_failed_entries,
                custrecord_shm_type: oFields.custrecord_shm_type,
                custrecord_shm_created: oFields.custrecord_shm_created,
                id: oFields.id,
            })
    }

    DisplayList();
}
 

function GetDbs() {
    var vURL = vNetSuiteURL + '&mode=GetDbs&callback=GetDbsCallBack';// 'http://shmwebservice.esided.com/API/GetDbs?callback=GetDbsCallBack';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (oData) {
            oDbs = oData;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetDbsCallBack(oData) {
    oDbs = oData;
    LoadDatabaseCombo();
}

function LoadDatabaseCombo() {
    $("[id='ddlDatabase']").off("chang");
    $("[id='ddlDatabase']").empty();
    $("[id='ddlLocation']").off("chang");
    $("[id='ddlLocation']").empty();
    $("[id='ddlLocation']").append("<option value='all'>All</option>");
    $("[id='ddlDatabase']").append("<option value='all'>All</option>");

    if ($("[id = 'ddlType'] option:selected").val() != "all") {
        for (i = 0; i < oDbs.length; i++) {
            $("#ddlDatabase").append("<option value='" + oDbs[i].name + "'>" + oDbs[i].name + "</option>");
        };

        $("[id='ddlDatabase']").off("chang").on("change", function () {
            LoadLocationCombo($("[id='ddlDatabase']").val());
            ClearListing();
        });
        LoadLocationCombo($("[id='ddlDatabase']").val());
    }
    $("[id='ddlLocation']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalYear']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalPeriod']").off("chang").on("change", function () {
        ClearListing()
    });
}

function GetMTIDbs() {

    var vURL = vNetSuiteURL + '&mode=GetRecordType&recordtype=customrecordnco_mytaskit_db_settings&callback=GetMTIDbsCallBack';
    $.ajax({
        type: "GET",
        url: vURL,
        dataType: "jsonp",
        success: function (oData) {
            oDbs = oData;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }
    });
}

function GetMTIDbsCallBack(pData) {
    $("#wait").css("display", "none");

    for (i = 0; i < pData.length; i++) {
        oMTIDbs.push({
            'Port': pData[i].fields.custrecordcustrecordnco_mytaskit_port,
            'Location': pData[i].fields.custrecordnco_mytaskit_location,
            'Company': pData[i].fields.custrecordnco_mytaskit_company,
            'id': pData[i].id,
        });
    };

}

function LoadMTIDatabaseCombo() {
    $("[id='ddlDatabase']").off("chang");
    $("[id='ddlDatabase']").empty();
    $("[id='ddlLocation']").off("chang");
    $("[id='ddlLocation']").empty();
    $("[id='ddlDatabase']").append("<option value='all'>All</option>");
    $("[id='ddlLocation']").append("<option value='all'>All</option>");

    for (i = 0; i < oMTIDbs.length; i++) {
        $("[id='ddlDatabase']").append("<option value='" + oMTIDbs[i].id + "'>" + oMTIDbs[i].Company + "</option>");
    };
    

    if ($("[id = 'ddlType'] option:selected").val() != "all") {
        $("[id='ddlDatabase']").val("3").attr("selected", "selected");

        $("[id='ddlDatabase']").off("chang").on("change", function () {
            LoadMTILocationCombo($("[id='ddlDatabase']").val());
            ClearListing();
        });
        LoadMTILocationCombo($("[id='ddlDatabase']").val());
    }
    $("[id='ddlLocation']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalYear']").off("chang").on("change", function () {
        ClearListing()
    });

    $("[id='ddlFiscalPeriod']").off("chang").on("change", function () {
        ClearListing()
    });
}

function LoadMTILocationCombo(vDbname) {
    $("[id='ddlLocation']").off("chang");
    $("[id='ddlLocation']").empty();
    $("[id='ddlLocation']").append("<option value='all'>All</option>");

    var oLoaction = Enumerable.From(oMTIDbs).Where(function (x) {
        return x.id == vDbname
    }).Select(function (x) {
        return x.Location
    }).ToArray();
    oLoaction = $.unique(oLoaction);
    

    if ($("[id = 'ddlType'] option:selected").val() != "all") {
        for (i = 0; i < oLoaction.length; i++) {
            $("[id='ddlLocation']").append("<option value='" + oLoaction[i] + "'>" + oLoaction[i] + "</option>");
        };
    }

}

function DisplayList() {
    setPagingAdvanced();
    $("[id = 'div_Journal_Entries_list']").html('');
    $("[id = 'div_Journal_Entries_list']").setTemplateURL(vSHM_Import_log_TemplateURL);
    $("[id = 'div_Journal_Entries_list']").processTemplate({ data: oJELogs });
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'right'
    });
    $('table table').hide();
    $('td > a').on('click', function (e) {
        e.preventDefault();

        if ($(this).closest('tr').next().find('table:first').is(":visible")) {
            $(this).closest('tr').next().find('table:first').fadeOut(100);
            $(this).find('img').attr('src', vDetailIcon_plus);
        }
        else {
            $(this).closest('tr').next().find('table:first').fadeIn(100);
            $(this).find('img').attr('src', vDetailIcon_minus);
        }
    });


    $('a#file_id').click(function () {
        var vFileId = $(this).attr("data-id");
        window.open(vDetail_viwe_log_template + 'fileid=' + vFileId, 'shm-import-queue', 'toolbar=no, menubar=no, resizable=yes');
        return false;
    });
    $(".pagination a").off("click").on("click", function () {

        if ($(this).attr('data-id') >-1 && $(this).attr('data-id') < oPageRanges.length) {
            vPageNo = $(this).attr('data-id');
            $("#wait").css("display", "");
            GetLogListClick();
        }
    });

    $("#wait").css("display", "none");
}



function ClearListing() {
    $("[id = 'div_Journal_Entries_list']").html('');
    //ToggleBtns(false);
}

function LoadLocationCombo(vDbname) {
    $("[id='ddlLocation']").off("chang");
    $("[id='ddlLocation']").empty();
    $("[id='ddlLocation']").append("<option value='all'>All</option>");
    var oLoaction = Enumerable.From(oDbs).Where(function (x) {
        return x.name == vDbname
    }).Select(function (x) {
        return x.locations
    }).ToArray()[0];

    
    if ($("[id = 'ddlType'] option:selected").val() != "all") {
        for (i = 0; i < oLoaction.length; i++) {
            $("[id='ddlLocation']").append("<option value='" + oLoaction[i] + "'>" + oLoaction[i] + "</option>");
        };
    }

}

function GetLogListClick() {
    var vFromDate = $("[id = 'FromDate']").val();
    var vToDate = $("[id = 'ToDate']").val();

    if (vFromDate.length > 0 && vToDate.length > 0) {
        vFromDate = new Date(vFromDate);
        vToDate = new Date(vToDate);
        if (!isNaN(vFromDate.getTime())) {
            vFromDate.setDate(vFromDate.getDate() - 1);
            var dd = vFromDate.getDate();
            var mm = vFromDate.getMonth() + 1;
            var y = vFromDate.getFullYear();
            vFromDate = mm + '/' + dd + '/' + y;

        } else {
            alert("Invalid FromDate");
        }
        if (!isNaN(vToDate.getTime())) {
            vToDate.setDate(vToDate.getDate() + 1);
            var dd = vToDate.getDate();
            var mm = vToDate.getMonth() + 1;
            var y = vToDate.getFullYear();
            vToDate = mm + '/' + dd + '/' + y;

        } else {
            alert("Invalid FromDate");
        }
    }

    GetGLLogs($("[id = 'ddlDatabase'] option:selected").val(), $("[id = 'ddlLocation'] option:selected").val(), $("[id = 'ddlFiscalYear'] option:selected").val(), $("[id = 'ddlFiscalPeriod'] option:selected").val(), vFromDate, vToDate, $("[id = 'ddlType'] option:selected").val());

}

$(document).ready(function () {
    GetDbs();
    GetMTIDbs();
    $("[id='btnViewList']").off("click").on("click", function () {
        vPageNo = 0;
        GetLogListClick();
    });

    //$("[id='btnViewList']").click();

    $("[id='ddlType']").off("chang").on("change", function () {
        if ($("[id = 'ddlType'] option:selected").val() != "all") {
            $("[id='ddlDatabase']").prop("disabled", false); 
            $("[id='ddlLocation']").prop("disabled", false);
            if ($("[id = 'ddlType'] option:selected").val() == "fsm") {
                LoadDatabaseCombo();
            } else {
                LoadMTIDatabaseCombo();
            }
        } else {
            $("[id='ddlDatabase']").off("chang");
            $("[id='ddlDatabase']").empty();
            $("[id='ddlLocation']").off("chang");
            $("[id='ddlLocation']").empty();
            $("[id='ddlLocation']").append("<option value='all'>All</option>");
            $("[id='ddlDatabase']").append("<option value='all'>All</option>");
            $("[id='ddlDatabase']").prop("disabled", true);
            $("[id='ddlLocation']").prop("disabled", true);
        }
    });
    if ($("[id = 'ddlType'] option:selected").val() == "all") {
        $("[id='ddlDatabase']").off("chang");
        $("[id='ddlDatabase']").empty();
        $("[id='ddlLocation']").off("chang");
        $("[id='ddlLocation']").empty();
        $("[id='ddlLocation']").append("<option value='all'>All</option>");
        $("[id='ddlDatabase']").append("<option value='all'>All</option>");
        $("[id='ddlDatabase']").prop("disabled", true);
        $("[id='ddlLocation']").prop("disabled", true);
    }
});



function DisplayError(pErrorMessage) {
   // alert(pErrorMessage);

}

function getFormattedDate(pDate) {
    var d = pDate.slice(0, 10).split('-');
    if (d.length > 1) {
        var year = d[0];
        var day = d[2];
        var month = d[1];
        //d[1] + '/' + d[2] + '/' + d[0];
        return month + '/' + day + '/' + year;
    }
    return pDate
}
var vStartPageNumbersFrom = 0;
var vStopPageNumbersAt = 0;

function setPagingAdvanced() {
    var vPagesRangeToDisaply = 5;

    if (vPagesRangeToDisaply % 2 != 0) {
        vPagesRangeToDisaply++;
    }
    vPageNo = parseInt(vPageNo);
    //Half the number of pages to output, this is the quantity either side.
    var vPagesToOutputHalfed = vPagesRangeToDisaply / 2;
    //The first page number to output.
    vStartPageNumbersFrom = vPageNo+1 - vPagesToOutputHalfed;
    //The last page number to output.
    vStopPageNumbersAt = vPageNo+1 + vPagesToOutputHalfed;;

    if (vStartPageNumbersFrom < 1) {
        vStartPageNumbersFrom = 1;
        //As page numbers are starting at one, output an even number of pages.
        vStopPageNumbersAt = vPagesRangeToDisaply;
    }
    //Make sure the stop page number isn’t after the last available page.
    if (vStopPageNumbersAt > vPageCount) {
        vStopPageNumbersAt = vPageCount;
    }
    //Make sure we’re showing a full page range.
    if ((vStopPageNumbersAt - vStartPageNumbersFrom) < vPagesRangeToDisaply) {
        vStartPageNumbersFrom = vStopPageNumbersAt - vPagesRangeToDisaply;
        if (vStartPageNumbersFrom < 1) {
            vStartPageNumbersFrom = 1;
        }
    }
 
}