﻿/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/http', 'N/https', 'N/search', 'N/record', 'N/task', 'N/file'],
/**
 * @param {email} email
 * @param {runtime} runtime
 * @param {serverWidget} serverWidget
 */
function (http, https, search, record, task, file) {

    function searchAll(recordType, columns) {
        var finalSearchResults = [];
        var pageLength = 1000;
        var index = 0;
        var start, end, page;

        var results = search.create({
            type: recordType,
            columns: columns
        }).run();
        do {
            start = index * pageLength;
            end = (index + 1) * pageLength;
            page = results.getRange({
                start: start,
                end: end
            });
            if (!!page && page.length > 0)
                finalSearchResults = finalSearchResults.concat(page);
            index++;
        } while (!!page && page.length == pageLength);
        return finalSearchResults;
    }

    function getAccounts() {
        var resultSet = searchAll('account', 'number')
        log.debug({ title: 'Accounts found', details: resultSet.length });

        return resultSet.reduce(function (hash, result) {
            hash[result.getValue('number')] = result.id;
            return hash;
        }, {});
    }

    function getAccountingPeriods() {
        var resultSet = searchAll('accountingperiod', 'periodname')
        //log.debug({ title: 'accountingperiod found', details: resultSet.length });
        return resultSet.reduce(function (hash, result) {
            hash[result.getValue('periodname')] = result.id;
            return hash;
        }, {});
    }

    function getDepartments() {
        var resultSet = searchAll('Department', 'name')
        //log.debug({ title: 'Department found', details: resultSet.length });
        return resultSet.reduce(function (hash, result) {
            hash[result.getValue('name')] = result.id;
            return hash;
        }, {});
    }

    function getClassification() {
        var resultSet = searchAll('classification', 'name')
        //log.debug({ title: 'Department found', details: resultSet.length });
        return resultSet.reduce(function (hash, result) {
            hash[result.getValue('name')] = result.id;
            return hash;
        }, {});
    }
    function getJournalEntry() {
        var resultSet = searchAll('journalentry', 'custbody_shm_source_id')
        //log.debug({ title: 'Department found', details: resultSet.length });
        return resultSet.reduce(function (hash, result) {
            hash[result.getValue('custbody_shm_source_id')] = result.id;
            return hash;
        }, {});
    }

    function getRecordArray(pRecordName, pFieldName) {
        var resultSet = searchAll(pRecordName, pFieldName)
        return resultSet.reduce(function (hash, result) {
            hash[result.getValue(pFieldName)] = result.id;
            return hash;
        }, {});
        return resultSet;
    }


    function RecordExist(pRecordName, pFieldName, pFieldValue) {
        var ret = 0;
        search.create({
            type: pRecordName,
            filters: [{ name: pFieldName, operator: 'is', values: [pFieldValue] }],
            columns: ['internalid']
        }).run().each(function (result) {
            ret = result.getValue({ name: 'internalid' });
            return false;
        });
        return ret;
    }

    function GetRecordType(pRecordName) {
        var mySearch = search.create({
            type: pRecordName,
            //filters: pFilters,
            columns: [{
                name: 'internalid', sort: search.Sort.DESC
            }]
        });
        var Results = [];
        var searchResult = mySearch.run().getRange({
            start: 0,
            end: 1000
        });
        for (var i = 0; i < searchResult.length; i++) {
            var internalid = searchResult[i].getValue({
                name: 'internalid'
            });
            var objRecord = record.load({
                type: pRecordName,
                id: internalid,
                isDynamic: true,
            })
            Results = Results.concat(objRecord);
        }
        return Results;
    }

    function GetJEImportlog(pLcationId, pDatabaseId, pFiscalYear, pFiscalPeriod) {
        var mySearch = search.create({
            type: pRecordName,
            filters: [
                      { name: 'custrecord_shm_location_id', operator: 'is', values: pLcationId },
                      { name: 'custrecordshm_database_id', operator: 'is', values: pDatabaseId },
                      { name: 'custrecordshm_fiscal_year', operator: 'is', values: pFiscalYear },
                      { name: 'custrecordshm_fiscal_period', operator: 'is', values: pFiscalPeriod }
            ],
            columns: [{
                name: 'internalid'
            }]
        });
        var Results = [];
        var searchResult = mySearch.run().getRange({
            start: 0,
            end: 1000
        });
        for (var i = 0; i < searchResult.length; i++) {
            var internalid = searchResult[i].getValue({
                name: 'internalid'
            });
            var objRecord = record.load({
                type: pRecordName,
                id: internalid,
                isDynamic: true,
            })
            Results = Results.concat(objRecord);
        }
        return Results;
    }

    function getLoadRecord(pRecordName, pInternalid) {
        var objRecord = record.load({
            type: pRecordName,
            id: pInternalid,
            isDynamic: true,
        })
        return objRecord;
    }

    function GetFSMData(context) {
        var database = context.request.parameters.database;
        var location = context.request.parameters.location;
        var fiscalyear = context.request.parameters.fiscalyear;
        var fiscalperiod = context.request.parameters.fiscalperiod;

        if (database == "" || location == "" || fiscalyear == "" || fiscalperiod == "") {
            throw 'Error getting data. Code: database or Location or fiscalyear or fiscalperiod is missing ';
        } else {
            var response = http.get({
                url: 'http://shmwebservice.esided.com/API/GLEntries?Database=' + database + '&Location=' + location + '&FiscalYear=' + fiscalyear + '&FiscalPeriod=' + fiscalperiod
            });
        }


        if (response.code != 200)
            throw 'Error getting data. Code:' + response.code
        return response.body
    }

    function GetDbs(context) {

        var response = http.get({
            url: 'http://shmwebservice.esided.com/API/GetDbs'
        });

        if (response.code != 200)
            throw 'Error getting data. Code:' + response.code
        return response.body
    }

    function GetMTIData(context, vLocationId, vPort, vToken) {
        var vId = context.request.parameters.database;
        var location = context.request.parameters.location;
        var beginEffDate = context.request.parameters.beginEffDate;
        var endEffDate = context.request.parameters.endEffDate;

        var oRecord = getLoadRecord("customrecordnco_mytaskit_db_settings", vId);

        var vPort = oRecord.getValue({ fieldId: 'custrecordcustrecordnco_mytaskit_port' });
        var vToken = oRecord.getValue({ fieldId: 'custrecordnco_mytaskit_token' });
        var vLocationId = oRecord.getValue({ fieldId: 'custrecordnco_mytaskit_location' });


        //6152
        var vURL = 'https://mytaskit.byy.com:' + vPort + '/api/v2/GL/RetrieveJournalTransactions?PostedOnly=true&BeginEffDate=' + beginEffDate + '&EndEffDate=' + endEffDate
        var vAutToken = "Bearer " + vToken;


        if (vPort == "" || beginEffDate == "" || endEffDate == "" || location == "") {
            throw 'Error getting data. Code: database or location or fiscalyear or fiscalperiod is missing ';
        } else {
            var response = https.get({
                url: vURL,
                headers: {
                    "Authorization": vAutToken
                }

            });
        }

        if (response.code != 200)
            throw 'Error getting data. Code:' + response.code
        return response.body
    }

    function FSM_JE_Transform(lines) {
        var periods = getAccountingPeriods();
        var oLocations = getClassification();
        log.debug({ title: 'Accounting Periods', details: JSON.stringify(periods) });

        var accounts = getAccounts();

        //log.debug({ title: 'Accounts', details: JSON.stringify(accounts) });
        var hash = lines.reduce(function (hash, line) {
            var tranid = line.tranid;
            var accParts = line.account.split('-');
            hash[tranid] = hash[tranid] || {
                custbody_shm_integ_id: line.DB_Name + '-' + line.tranid,
                custbody_shm_db_name: line.DB_Name,
                trandate: new Date(line.posteddate),
                postingperiod: periods[monthToPeriod(line.period, line.fiscalyear)],
                //custbody_nco_interco_owner: accParts[1] + accParts[2],
                custbody_nco_interco_owner: oLocations[accParts[1] + accParts[2]],
                custbody_shm_jname: line.jname,
                description: line.description,
                isvalid: 0,
                validation_error: "",
                isimported: 0,
                import_id: 0,
                import_isvalid: 0,
                import_error: "",
                debit: 0,
                credit: 0,
                balance: 0,
                department: accParts[3],
                lines: []
            }
            hash[tranid].lines.push({
                account: accounts[accParts[0]],
                debit: line.debit,
                credit: line.credit,
                department: accParts[3],
                location: accParts[1] + accParts[2],//hash[tranid].custbody_nco_interco_owner,
                clss: hash[tranid].custbody_nco_interco_owner,
                memo: line.linedescription
            })
            hash[tranid].debit = hash[tranid].debit + line.debit;
            hash[tranid].credit = hash[tranid].credit + line.credit;
            hash[tranid].balance = hash[tranid].credit - hash[tranid].debit;
            return hash;
        }, {})
        var objs = [];
        for (var tranid in hash)
            objs.push(hash[tranid]);
        return objs;


    }

    function MTI_JE_Transform(mtiJEs, periods, vId) {

        var oRecord = getLoadRecord("customrecordnco_mytaskit_db_settings", vId);

        var vPort = oRecord.getValue({ fieldId: 'custrecordcustrecordnco_mytaskit_port' });
        var vLocationId = oRecord.getValue({ fieldId: 'custrecordnco_mytaskit_location' });

        ////log.debug({ title: 'Posting Date ' + mtiJE.DatePosted + ', parsed date ' + postingDate, details: 'periodName ' + periodName });
        var t = [];
        mtiJEs.map(function (mtiJE) {
            var postingDate = new Date(mtiJE.AcctDate);
            var periodName = monthToPeriodMTI(postingDate.getMonth(), postingDate.getFullYear());
            var objRecord = {
                custbody_shm_integ_id: vPort + '-' + mtiJE.Id,
                custbody_shm_source_id: mtiJE.Id,
                custbody_shm_db_name: vPort,
                trandate: mtiJE.AcctDate,
                postingperiod: periods[periodName],
                custbody_nco_interco_owner: vLocationId,
                custbody_shm_jname: "",// mtiJE.custbody_shm_jname,
                memo: mtiJE.Description,
                description: mtiJE.Description,

                isvalid: 0,
                validation_error: "",
                isimported: 0,
                import_id: 0,
                import_isvalid: 0,
                import_error: "",
                debit: 0,
                credit: 0,
                balance: 0

            };
            objRecord.lines = mtiJE.TransactionLines.map(function (line, i) {
                var accParts = line.GLAccountAltDesc.split(',');//3559,112220,00 -> internalid, acctnum, dept
                var obj = {
                    account: accParts[0],
                    clss: vLocationId,
                    location: vLocationId,
                    department: accParts[2],
                    memo: line.LineComment,
                    debit: 0,
                    credit: 0
                };
                if (!!line.DebitAmount)
                    obj.debit = +line.DebitAmount;
                if (!!line.CreditAmount)
                    obj.credit = +line.CreditAmount;
                objRecord.debit = objRecord.debit + line.DebitAmount;
                objRecord.credit = objRecord.credit + line.CreditAmount;
                
                return obj;
            });

            objRecord.balance = objRecord.credit - objRecord.debit;
            t.push(objRecord);

        });
  
        return t;

    }

    function monthToPeriod(month, fiscalYear) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[+month - 1] + ' ' + fiscalYear.trim();
    }


    function monthToPeriodMTI(month, fiscalYear) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[+month] + ' ' + fiscalYear;
    }

    function SHM_JE_Import_validation(oJE, pType) {
        var oAccounts = getRecordArray("account", "internalid");
        var oDepartments = getDepartments();
        var oLocations = getRecordArray("classification", "internalid"); //getClassification();
        var vLocationError = "";
        var vLocation = oJE[0].custbody_nco_interco_owner;
        if (oLocations[vLocation] == null) {
            vLocationError = 'Location("' + vLocation + '") does not exist';
        }

        for (var i = 0; i < oJE.length; i++) {

            var vDebitAmount = 0;
            var vCreditAmount = 0;
            oJE[i].validation_error = vLocationError;



            for (c = 0; c < oJE[i].lines.length; c++) {
                var vAccount = oJE[i].lines[c].account;
                var vDepartment = oJE[i].lines[c].department;
                var vLocation = oJE[i].lines[c].clss;
                //if (oJE[i].validation_error == 0) {
                if (oDepartments[vDepartment] == null) {
                    if (oJE[i].validation_error.length > 0) oJE[i].validation_error = oJE[i].validation_error + ',';
                    oJE[i].validation_error = oJE[i].validation_error + '  Department("' + vDepartment + '") does not exist';
                }
                if (oAccounts[vAccount] == null) {
                    if (oJE[i].validation_error.length > 0) oJE[i].validation_error = oJE[i].validation_error + ',';
                    oJE[i].validation_error = oJE[i].validation_error + 'Account("' + vAccount + '")  does not exist';
                }
                //        		if(periods[monthToPeriod(oLine.period, oLine.fiscalyear)]==null){
                //    				if(oLine.Error.length>0) oLine.Error = oLine.Error + ',';
                //    				lines[i].Error = lines[i].Error + ' Peroid do not exist';
                //    			}        		
                // }
                vDebitAmount = vDebitAmount + oJE[i].lines[c].debit;
                vCreditAmount = vCreditAmount + oJE[i].lines[c].credit;
            }
            if (oJE[i].validation_error.length > 0) {
                oJE[i].isvalid = 1;
            }
            oJE[i].debit = vDebitAmount;
            oJE[i].credit = vCreditAmount;
            oJE[i].balance = vCreditAmount - vDebitAmount;

            if (oJE[i].balance != 0) {
                if (oJE[i].validation_error.length > 0) oJE[i].validation_error = lines[i].validation_error + ',';
                oJE[i].validation_error = ' Balance is not equal to 0("' + oJE[i].balance + '")';
            }
        }

        return oJE;
    }


    function Event_JournalEntrieslist(context) {
        var callback = context.request.parameters.callback;
        var lines = JSON.parse(GetFSMData(context));
        var oJE = FSM_JE_Transform(lines, true);
        //oJE = oJE.slice(0, 2);
        context.response.write(callback + "(" + JSON.stringify(oJE) + ")");

    }

    function Event_GetDbs(context) {
        var callback = context.request.parameters.callback;
        var data = GetDbs(context);
        context.response.write(callback + "(" + data + ")");
    }

    function Event_MTIJournalEntrieslist(context) {
        var callback = context.request.parameters.callback;
        var vId = context.request.parameters.database;
        var lines = JSON.parse(GetMTIData(context));
        var periods = getAccountingPeriods();
        var oJE = MTI_JE_Transform(lines, periods, vId);
        //oJE = oJE.slice(0, 2);

        context.response.write(callback + "(" + JSON.stringify(oJE) + ")");
        
    }

    function Event_GetRecordType(context) {
        var callback = context.request.parameters.callback;
        var recordtype = context.request.parameters.recordtype;
        var ret = GetRecordType(recordtype);
        context.response.write(callback + "(" + JSON.stringify(ret) + ")");

    }

    function Event_RecordExist(context) {
        var recordtype = context.request.parameters.recordtype;
        var fname = context.request.parameters.fname;
        var fvalue = context.request.parameters.fvalue;
        context.response.write(JSON.stringify(RecordExist(recordtype, fname, fvalue)));
    }

    function Event_getRecordArray(context) {
        var recordtype = context.request.parameters.recordtype;
        var fname = context.request.parameters.fname;
        context.response.write(JSON.stringify(getRecordArray(recordtype, fname)));
    }

    function Event_FSMValidate(context) {
        var callback = context.request.parameters.callback;
        var lines = JSON.parse(GetFSMData(context));
        var oJE = FSM_JE_Transform(lines, true);
        //oJE = oJE.slice(0, 2);
        var oJEValidatedData = SHM_JE_Import_validation(oJE, "fsm")
        context.response.write(callback + "(" + JSON.stringify(oJEValidatedData) + ")");
    }

    function Event_MTIDataValidate(context) {

        var vId = context.request.parameters.database;
        var callback = context.request.parameters.callback;
        var periods = getAccountingPeriods();
        var lines = JSON.parse(GetMTIData(context));
        var oJE = MTI_JE_Transform(lines, periods, vId);

        //oJE = oJE.slice(0, 2);
        oJE = SHM_JE_Import_validation(oJE, "mti");
        context.response.write(callback + "(" + JSON.stringify(oJE) + ")");
    }

    function Event_FSMImport(context) {
        var vDatabase = context.request.parameters.database;
        var vLocation = context.request.parameters.location;
        var vFiscalyear = context.request.parameters.fiscalyear;
        var vFiscalperiod = context.request.parameters.fiscalperiod;
        var vDatabase_name = context.request.parameters.databasename;
        var vLocation_name = context.request.parameters.locationname;
 

        var vImportKey = Math.random().toString().slice(2);

        var callback = context.request.parameters.callback;

        var lines = JSON.parse(GetFSMData(context, 1));

        var oJE = FSM_JE_Transform(lines, true);
        //oJE = oJE.slice(0, 2);
        var oJEValidatedData = SHM_JE_Import_validation(oJE, "fsm")

        var mapReduceScriptId = 139;//hilass demo 147;// "customscript126";
        var mrTask = task.create({
            taskType: task.TaskType.MAP_REDUCE,
            scriptId: mapReduceScriptId,
            params: {
                custscript_shm_jedata: oJEValidatedData,
                custscript_shm_dbid: vDatabase,
                custscript_shm_location: vLocation,
                custscript_shm_fyear: vFiscalyear,
                custscript_shm_fperiod: vFiscalperiod,
                custscript_shm_unique_number: vImportKey,
                custscript_shm_type: "FSM",
                custscript_shm_database_name: vDatabase_name,
                custscript_shm_location_name: vLocation_name,
            },
            deploymentId: 1
        });
        //context.response.write(JSON.stringify(mrTask));
        var mrTaskId = mrTask.submit();

        var oError = {
            status: 0,
            message: ''
        }

        var taskStatus = task.checkStatus(mrTaskId);

        if (taskStatus.status === 'FAILED') {
            oError.status = 1;
            oError.message = "Failed to run import script";
        } else {
            oError.status = 0;
            oError.message = "Import is queue now";
        }

        context.response.write(callback + "(" + JSON.stringify(oError) + ")");
    }


    function Event_MTIImport(context) {
        var vDatabase = context.request.parameters.database;
        var vLocation = context.request.parameters.location;
        var vFiscalyear = context.request.parameters.fiscalyear;
        var vFiscalperiod = context.request.parameters.fiscalperiod;
        var vDatabase_name = context.request.parameters.databasename;
        var vLocation_name = context.request.parameters.locationname;

        var vImportKey = Math.random().toString().slice(2);

        var periods = getAccountingPeriods();
        var callback = context.request.parameters.callback;
        var vId = context.request.parameters.database;

        var lines = JSON.parse(GetMTIData(context));

        var oJE = MTI_JE_Transform(lines, periods, vId);
        var oJEValidatedData = SHM_JE_Import_validation(oJE, "mti")
        var mapReduceScriptId = 139;//hilass147;// "customscript126";

        var mrTask = task.create({
            taskType: task.TaskType.MAP_REDUCE,
            scriptId: mapReduceScriptId,
            params: {
                custscript_shm_jedata: oJEValidatedData,
                custscript_shm_dbid: vDatabase,
                custscript_shm_location: vLocation,
                custscript_shm_fyear: vFiscalyear,
                custscript_shm_fperiod: vFiscalperiod,
                custscript_shm_unique_number: vImportKey,
                custscript_shm_type: "MTI",
                custscript_shm_database_name: vDatabase_name,
                custscript_shm_location_name: vLocation_name,
            },
            deploymentId: 1
        });

        var mrTaskId = mrTask.submit();

        var oError = {
            status: 0,
            message: ''
        }

        var taskStatus = task.checkStatus(mrTaskId);

        if (taskStatus.status === 'FAILED') {
            oError.status = 1;
            oError.message = "Failed to run import script";
        } else {
            oError.status = 0;
            oError.message = "Import is queue now";
        }

        context.response.write(callback + "(" + JSON.stringify(oError) + ")");
      

    }


    function Event_GetImportLogsList(context) {
        var callback = context.request.parameters.callback;
        var recordtype = context.request.parameters.recordtype;

        var vDatabase = context.request.parameters.database;
        var vLocation = context.request.parameters.location;
        var vFiscalyear = context.request.parameters.fiscalyear;
        var vFiscalperiod = context.request.parameters.fiscalperiod;
        var vFromDate = context.request.parameters.FromDate;
        var vToDate = context.request.parameters.ToDate;
        var vPageNo = context.request.parameters.pageno;
        var vType = context.request.parameters.Type;

        if (!vPageNo) {
            vPageNo = 0;
        }


        var oFilters = [];
        var vCount = 0;
        if (vDatabase != "all") {
            oFilters[vCount++] = ['custrecord_shm_database_id', search.Operator.IS, vDatabase];
        }
        if (vLocation != "all") {
            if (vCount > 0) oFilters[vCount++] = 'and';
            oFilters[vCount++] = ['custrecord_shm_location_id', search.Operator.IS, vLocation];
        }
        if (vFiscalyear != "all") {
            if (vCount > 0) oFilters[vCount++] = 'and';
            oFilters[vCount++] = ['custrecord_shm_fiscal_year', search.Operator.IS, vFiscalyear];
        }
        if (vFiscalperiod != "all") {
            if (vCount > 0) oFilters[vCount++] = 'and';
            oFilters[vCount++] = ['custrecord_shm_fiscal_period', search.Operator.IS, vFiscalperiod];
        }
        if (vType != "all") {
            if (vCount > 0) oFilters[vCount++] = 'and';
            oFilters[vCount++] = ['custrecord_shm_type', search.Operator.IS, vType];
        }
        if (vFromDate.length > 0 && vToDate.length > 0) {
            if (vCount > 0) oFilters[vCount++] = 'and';
            oFilters[vCount++] = ['custrecord_shm_created', search.Operator.BETWEEN, vFromDate, vToDate];
        }

        var oSearch = search.create({
            type: "customrecord_shm_ge_import_log",
            filters: oFilters,
            columns: [{
                name: 'internalid', sort: search.Sort.DESC
            }]
        });

        var Results = [];

        var oPagedData = oSearch.runPaged({
            pageSize: 10
        });

        var vPageRanges = oPagedData.pageRanges;
        var vPageSize = oPagedData.pageSize;

        if (oPagedData.count > 0) {
            var oPage = oPagedData.fetch({ index: vPageNo });
            oPage.data.forEach(function (result) {
                var internalid = result.getValue({
                    name: 'internalid'
                });
                var objRecord = record.load({
                    type: "customrecord_shm_ge_import_log",
                    id: internalid,
                    isDynamic: true,
                })
                Results = Results.concat(objRecord);
            });
        }
        var oResults = { Total: oPagedData.count, PageSize: vPageSize, PageNo: vPageNo, PageRanges: vPageRanges, Data: JSON.stringify(Results) }

        context.response.write(callback + "(" + JSON.stringify(oResults) + ")");
    }

    function Event_GetImportLogFile(context) {
        var callback = context.request.parameters.callback;
        var vFileId = context.request.parameters.fileid;
        var fileObj = file.load({
            id: vFileId
        });

        context.response.write(callback + "(" + fileObj.getContents() + ")");
    }


    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
        if (context.request.method === 'GET') {
            try {

                var mode = context.request.parameters.mode;

                switch (mode) {
                    case "JournalEntrieslist":
                        Event_JournalEntrieslist(context);
                        break
                    case "GetDbs":
                        Event_GetDbs(context);
                        break
                    case "MTIJournalEntrieslist":
                        Event_MTIJournalEntrieslist(context);
                        break;

                    case "GetRecordType":
                        Event_GetRecordType(context);
                        break;
                    case "RecordExist":
                        Event_RecordExist(context);
                        break;

                    case "getRecordArray":
                        Event_getRecordArray(context);
                        break;
                    case "FSMValidate":
                        Event_FSMValidate(context);
                        break

                    case "MTIDataValidate":
                        Event_MTIDataValidate(context);
                        break
                    case "FSMImport":
                        Event_FSMImport(context);
                        break
                    case "MTIImport":
                        Event_MTIImport(context);
                        break;
                    case "GetImportLogFile":
                        Event_GetImportLogFile(context);
                        break
                    case "GetImportLogsList":
                        Event_GetImportLogsList(context);
                        break;
                }
            } catch (err) {
                context.response.write("DisplayError(" + JSON.stringify(err) + ")");
            }

        } else {
            var request = context.request;

            var s = request.parameters.subject;

            context.response.write(s);
        }
    }

    return {
        onRequest: onRequest
    };

});
