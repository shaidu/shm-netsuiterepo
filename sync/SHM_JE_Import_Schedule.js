/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/file', 'N/runtime', 'N/record', 'N/search'],
    function (file, runtime, record, search) {
        var oJEList = [];
        var vLogFilename = "";

        var oImportLog = {
            custrecord_shm_location_id: "",
            custrecord_shm_database_id: "",
            custrecord_shm_fiscal_year: "",
            custrecord_shm_fiscal_period: "",
            custrecord_shm_import_status: 0,
            custrecord_shm_file_id: 0,
            custrecord_shm_file_url: 0,
            custrecord_shm_file_name: "",
            custrecord_shm_database_name: "",
            custrecord_shm_location_name: "",
            custrecord_shm_type: ""
        }

        var vImportLogId = 0;


        function create_import_log(obj) {
            var objRecord = record.create({
                type: "customrecord_shm_ge_import_log",
                isDynamic: true
            });
            objRecord.setValue({ fieldId: 'name', value: obj.custrecord_shm_location_id });
            objRecord.setValue({ fieldId: 'custrecord_shm_location_id', value: obj.custrecord_shm_location_id });
            objRecord.setValue({ fieldId: 'custrecord_shm_database_id', value: obj.custrecord_shm_database_id });
            objRecord.setValue({ fieldId: 'custrecord_shm_fiscal_year', value: obj.custrecord_shm_fiscal_year });
            objRecord.setValue({ fieldId: 'custrecord_shm_fiscal_period', value: obj.custrecord_shm_fiscal_period });

            objRecord.setValue({ fieldId: 'custrecord_shm_file_id', value: obj.custrecord_shm_file_id });
            objRecord.setValue({ fieldId: 'custrecord_shm_file_url', value: obj.custrecord_shm_file_url });
            objRecord.setValue({ fieldId: 'custrecord_shm_file_name', value: obj.custrecord_shm_file_name });

            objRecord.setValue({ fieldId: 'custrecord_shm_database_name', value: obj.custrecord_shm_database_name });
            objRecord.setValue({ fieldId: 'custrecord_shm_location_name', value: obj.custrecord_shm_location_name });

            objRecord.setValue({ fieldId: 'custrecord_shm_total_entries', value: obj.custrecord_shm_total_entries });
            objRecord.setValue({ fieldId: 'custrecord_shm_total_successfull_entries', value: obj.custrecord_shm_total_successfull_entries });
            objRecord.setValue({ fieldId: 'custrecord_shm_total_failed_entries', value: obj.custrecord_shm_total_failed_entries });
            objRecord.setValue({ fieldId: 'custrecord_shm_type', value: obj.custrecord_shm_type });

            
     
            var recordId = objRecord.save({
                enableSourcing: false,
                ignoreMandatoryFields: false
            });
            return recordId;
        }

        //function update_Import_log(pImportLogId, pSuccess) {
        //    if (pImportLogId > 0) {
        //        var objRecord = record.load({
        //            type: "customrecord_shm_ge_import_log",
        //            id: pImportLogId,
        //            isDynamic: true,
        //        });

        //        var countSuccessfull = objRecord.getValue({ fieldId: 'custrecord_shm_total_successfull_entries' });
        //        var countFail = objRecord.getValue({ fieldId: 'custrecord_shm_total_failed_entries' });
        //        if (pSuccess > 0) {
        //            countSuccessfull = countSuccessfull + 1;
        //        } else {
        //            countFail = countFail + 1;
        //        }
        //        objRecord.setValue({ fieldId: 'custrecord_shm_total_successfull_entries', value: countSuccessfull });
        //        objRecord.setValue({ fieldId: 'custrecord_shm_total_failed_entries', value: countFail });
        //        var recordId = objRecord.save({
        //            enableSourcing: false,
        //            ignoreMandatoryFields: false
        //        });
        //    }
        //}

        function update_Import_log(pImportLogId, pSuccess, pFailed) {
            if (pImportLogId > 0) {
                var objRecord = record.load({
                    type: "customrecord_shm_ge_import_log",
                    id: pImportLogId,
                    isDynamic: true,
                });

                objRecord.setValue({ fieldId: 'custrecord_shm_total_successfull_entries', value: pSuccess });
                objRecord.setValue({ fieldId: 'custrecord_shm_total_failed_entries', value: pFailed });
                var recordId = objRecord.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: false
                });
            }
        }
        function RecordExist(pRecordName, pFieldName, pFieldValue) {
            var ret = 0;
            search.create({
                type: pRecordName,
                filters: [{ name: pFieldName, operator: 'is', values: [pFieldValue] }],
                columns: ['internalid']
            }).run().each(function (result) {
                ret = result.getValue({ name: 'internalid' });
                return false;
            });
            return ret;
        }

        function create_log_file(pName, pdata) {

            var fileObj = file.create({
                name: pName,
                fileType: file.Type.JSON,
                contents: pdata
            });
            fileObj.isOnline = true;
            //hillas demo fileObj.folder = 2387;
            fileObj.folder = 625;
            var fileId = fileObj.save();
            var fileObj = file.load({
                id: fileId
            });

            oImportLog.custrecord_shm_file_id = fileId;
            oImportLog.custrecord_shm_file_url = fileObj.url;
        }

        function create(obj) {
            var objRecord = record.create({
                type: record.Type.JOURNAL_ENTRY,
                isDynamic: true
            });

            objRecord.setValue({ fieldId: 'custbody_shm_integ_id', value: obj.custbody_shm_integ_id });
            objRecord.setValue({ fieldId: 'custbody_shm_source_id', value: obj.custbody_shm_source_id });
            objRecord.setValue({ fieldId: 'custbody_shm_db_name', value: obj.custbody_shm_db_name });
            objRecord.setValue({ fieldId: 'trandate', value: new Date(obj.trandate) });
            objRecord.setValue({ fieldId: 'postingperiod', value: obj.postingperiod });
            objRecord.setValue({ fieldId: 'custbody_nco_interco_owner', value: obj.custbody_nco_interco_owner });
            objRecord.setValue({ fieldId: 'custbody_shm_jname', value: obj.custbody_shm_jname });
            objRecord.setValue({ fieldId: 'memo', value: obj.memo });

            obj.lines.forEach(createLine);

            var recordId = objRecord.save({
                enableSourcing: false,
                ignoreMandatoryFields: false
            });
            log.debug({ title: 'Record created', details: 'Id: ' + recordId });
            return recordId;

            function createLine(line) {
                var createCredit = false;
                objRecord.setCurrentSublistValue({ sublistId: 'line', fieldId: 'account', value: line.account });
                objRecord.setCurrentSublistValue({ sublistId: 'line', fieldId: 'class', value: line.clss });
                objRecord.setCurrentSublistText({ sublistId: 'line', fieldId: 'department', text: line.department });
                objRecord.setCurrentSublistValue({ sublistId: 'line', fieldId: 'memo', value: line.memo });
                if (!!line.debit)
                    objRecord.setCurrentSublistValue({ sublistId: 'line', fieldId: 'debit', value: +line.debit });

                if (!!line.credit)
                    if (!!line.debit)
                        createCredit = true
                    else
                        objRecord.setCurrentSublistValue({ sublistId: 'line', fieldId: 'credit', value: +line.credit });

                objRecord.commitLine({
                    sublistId: 'line'
                });
                if (!!createCredit) {
                    line.debit = 0;
                    createLine(line)
                }
            }
        }

        function getInputData() {
            var vRecordsReceived = runtime.getCurrentScript().getParameter('custscript_shm_jedata');
            var vlocation = runtime.getCurrentScript().getParameter('custscript_shm_location');
            var vDbid = runtime.getCurrentScript().getParameter('custscript_shm_dbid');
            var vFYear = runtime.getCurrentScript().getParameter('custscript_shm_fyear');
            var vFPeriod = runtime.getCurrentScript().getParameter('custscript_shm_fperiod');
            var vUniqueNumber = runtime.getCurrentScript().getParameter('custscript_shm_unique_number');
            var vType = runtime.getCurrentScript().getParameter('custscript_shm_type');
            var vDatabase_name = runtime.getCurrentScript().getParameter('custscript_shm_database_name');
            var vLocation_name = runtime.getCurrentScript().getParameter('custscript_shm_location_name');

            var oRecordsReceived = JSON.parse(vRecordsReceived);

            vLogFileName = vType + "-" + vDbid + "-" + vlocation + "-" + vFYear + "-" + vFPeriod + '-' + vUniqueNumber + ".json";

            oImportLog.custrecord_shm_file_name = vLogFileName;
            oImportLog.custrecord_shm_location_id = vlocation;
            oImportLog.custrecord_shm_database_id = vDbid;
            oImportLog.custrecord_shm_fiscal_year = vFYear;
            oImportLog.custrecord_shm_fiscal_period = vFPeriod;
            oImportLog.custrecord_shm_database_name = vDatabase_name;
            oImportLog.custrecord_shm_location_name = vLocation_name;

            oImportLog.custrecord_shm_total_entries = oRecordsReceived.length;
            oImportLog.custrecord_shm_total_successfull_entries = 0;
            oImportLog.custrecord_shm_total_failed_entries = 0;
            oImportLog.custrecord_shm_type = vType;
           

            oImportLog.custrecord_shm_file_name = vLogFileName;

            create_log_file(vLogFileName, "[]");

            vImportLogId = create_import_log(oImportLog);
 
            // .filter(function(journalEntry){
            // 	return ['226131', '226742', '226278', '226151', '226141'].indexOf(journalEntry.Id) >= 0;
            // });

            //log.debug({ title: 'Records received', details: oRecordsReceived.length });

            return oRecordsReceived.map(function (journalEntry) {
                return JSON.stringify({ JE: journalEntry, ImportLogId: vImportLogId, FileName: vLogFileName });
            });

        }

        function reduce(context) {

            //log.debug({ title: 'reduce context ' + context.key, details: JSON.stringify(context) })

            var oContext = JSON.parse(context.values[0]);

            var je = oContext.JE;
            var vFilename = oContext.FileName;
            var vImportLogId = oContext.ImportLogId;
            var success = 0;
            try {

                if (je.validation_error == "" && je.balance == 0) {

                    log.debug({ title: 'Received ' + context.key, details: context.values[0] });
                    var tId = parseInt(RecordExist("journalentry", "custbody_shm_integ_id", je.custbody_shm_integ_id.trim()));
                    if (tId > 0) {
                        je.isimported = 0;
                        je.import_id = 0;
                        je.import_isvalid = 0;
                        je.import_error = "Fail To import due to duplciate"
                        // update_Import_log(vImportLogId, 0);
                        //log.debug({ title: 'Fail To import due to duplciate ' + tId });
                    }
                    else {
                        var id = create(je);
                        //var success = !!id;
                        if (id > 0) success = 1;
                        if (success) {
                            je.isimported = 1;
                            je.import_id = id;
                            je.import_isvalid = 1;
                            je.import_error = "";
                            log.debug({ title: 'Imported   ' + id });
                        } else {
                            je.isimported = 0;
                            je.import_id = 0;
                            je.import_isvalid = 0;
                            je.import_error = "Fail To import";
                            log.debug({ title: 'Fail To import   ' + id });
                        }

                        //update_Import_log(vImportLogId, success);

                    }
                } else {
                    //update_Import_log(vImportLogId, 0);
                }

            } catch (ex) {

                log.error({ title: 'Error in reducing ' + je.custbody_shm_integ_id, details: ex.message });
                je.isimported = 0;
                je.import_id = 0;
                je.import_isvalid = 0;
                je.import_error = ex.message;
                //update_Import_log(vImportLogId, 0);
            }
            oJEList.push(je);

            //update_Import_log(vImportLogId, success);
            //create_log_file(vFilename, JSON.stringify(oJEList));
            context.write(context.key, JSON.stringify({ FileName: vFilename, ImportLogId: vImportLogId, JE: je, Success: success }));
        }

        function summarize(summary) {
            var tempJE = [];
            var countSuccessfull = 0;
            var vFilename = "";
            var vSuccesfull = 0;
            var vFailed = 0;
            var vImportLogId = 0;
            summary.output.iterator().each(function (key, value) {
                // countSuccessfull += value == 'true' ? 0 : 1;
                var oContext = JSON.parse(value);
                var oJE = oContext.JE;
                var vSuccess = oContext.Success;

                if (vFilename.length == 0) {
                    vFilename = oContext.FileName;
                    vImportLogId = oContext.ImportLogId;
                }
                tempJE.push(oJE);
                //create_log_file("m-test-02.json", JSON.stringify(tempJE));
                //create_log_file(vFilename, JSON.stringify(tempJE));
                if (vSuccess == 1) {
                    vSuccesfull += 1;
                }
                else {
                    vFailed += 1;
                }
                log.debug({ title: 'summarize context ' + key, details: JSON.stringify(oJE) });
                log.debug({ title: 'vFilename ' + key, details: vFilename });

                return true;
            });
            //log.debug({ title: 'vFilename 1 ' + key, details: vFilename });
            
            //create_log_file("m-test-03.json", JSON.stringify(tempJE));
            create_log_file(vFilename , JSON.stringify(tempJE));
            update_Import_log(vImportLogId, vSuccesfull, vFailed);

            log.debug({ title: 'Records created successfully', details: countSuccessfull });
        }

        return {
            getInputData: getInputData,
            reduce: reduce,
            summarize: summarize
        };
    });